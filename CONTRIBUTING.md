# Contributing to this Document 
Thanks for your interest in contributing to the PEGO code. 

If you find a bug, please raise an issue [here](https://gitlab.com/agm/ece512/issues) first. Then create a merge request [here](https://gitlab.com/agm/ece512/merge_requests). 
