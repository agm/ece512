Please raise an [issue](https://gitlab.com/agm/ece512/issues) if you

- Find a mistake in lab notes
- Find a bug in the code
- Find a mistake Schematics 
- Find an error in this documentation 
- Need clarification for something in lab notes/code/schematics
- Would like to request a new feature
- Would like to request documentation

*Please use the appropriate labels when you raise an issue* 

**Note**: You have to create a GitLab account to raise an issue. 

If you'd like to edit the code in this repository to fix a bug or add a new feature, please refer to the [contributing guide](CONTRIBUTING.md). 