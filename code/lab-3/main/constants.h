/* 
Code for ECE 512 Labs 3-7
Date updated: 2019-05-23
For detailed documentation and explanation please visit https://gitlab.com/agm/ece512
*/

// Interrupt related 
#define SETBIT(ADDRESS,BIT) (ADDRESS |= (1<<BIT))
#define CLEARBIT(ADDRESS,BIT) (ADDRESS &= ~(1<<BIT))
#define CHECKBIT(ADDRESS,BIT) (ADDRESS & (1<<BIT))
#define TOGGLEBIT(ADDRESS,BIT) (ADDRESS ^= (1<<BIT))

//PWM related
unsigned int runDC;
#define DEADTIME    8
#define PERIOD      400   // 25kHz:320; 20kHz:400
#define DUTYRATIO   200
#define MAXTIMECOUNTDC 2000



// State machine and protection related
#define TRUE 1
#define FALSE 0
unsigned char switchstate;
unsigned int Run_state;

